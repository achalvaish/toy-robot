package cli;

import Resources.Messages;

import model.Robot;
import org.junit.*;
import org.junit.runner.JUnitCore;

import static org.junit.Assert.*;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;



public class CommandsControllerTest extends JUnitCore {

    Robot robot =new Robot();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testExecuteCommand_place_insufficient_params_no_direction() {
        String[] command={"place","1,2"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.INVALID_DIRECTION,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_place_insufficient_params_one_coordinate() {
        String[] command={"place","1"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.INSUFFICIENT_PARAMS,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_place_insufficient_params_invalid_x() {
        String[] command={"place","a,1,north"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.INSUFFICIENT_PARAMS,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_place_insufficient_params_invalid_y() {
        String[] command={"place","1,a,north"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.INSUFFICIENT_PARAMS,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_place_insufficient_params_x_out_of_table() {
        String[] command={"place","5,1,north"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.INVALID_X_PARAM,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_place_insufficient_params_y_out_of_table() {
        String[] command={"place","1,5,north"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.INVALID_Y_PARAM,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_move_x_valid_east(){
        String[] command;
        command= new String[]{"place", "1,4,east"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("2,4,EAST",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_move_y_valid_north(){
        String[] command;
        command= new String[]{"place", "1,3,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("1,4,NORTH",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_move_x_valid_west(){
        String[] command;
        command= new String[]{"place", "3,4,west"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("2,4,WEST",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_move_y_valid_south(){
        String[] command;
        command= new String[]{"place", "1,3,south"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("1,2,SOUTH",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_left(){
        String[] command;
        command= new String[]{"place", "2,4,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"left"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("2,4,WEST",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_right(){
        String[] command;
        command= new String[]{"place", "2,4,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"right"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("2,4,EAST",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_left_move_valid_west(){
        String[] command;
        command= new String[]{"place", "2,4,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"left"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("1,4,WEST",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_right_move_valid_east(){
        String[] command;
        command= new String[]{"place", "2,4,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"right"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("3,4,EAST",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_left_move_invalid_west(){
        String[] command;
        command= new String[]{"place", "0,4,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"left"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.END_OF_TABLE,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_right_move_invalid_east(){
        String[] command;
        command= new String[]{"place", "4,4,north"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"right"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.END_OF_TABLE,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_left_move_valid_north(){
        String[] command;
        command= new String[]{"place", "2,3,east"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"left"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("2,4,NORTH",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_right_move_valid_south(){
        String[] command;
        command= new String[]{"place", "2,4,east"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"right"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"report"};
        CommandController.executeCommand(command,robot);
        assertEquals("2,3,SOUTH",outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_left_move_invalid_north(){
        String[] command;
        command= new String[]{"place", "0,4,east"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"left"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.END_OF_TABLE,outContent.toString().trim());
    }

    @Test
    public void testExecuteCommand_right_move_invalid_south(){
        String[] command;
        command= new String[]{"place", "4,0,east"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"right"};
        CommandController.executeCommand(command,robot);
        command = new String[]{"move"};
        CommandController.executeCommand(command,robot);
        assertEquals(Messages.END_OF_TABLE,outContent.toString().trim());
    }
}