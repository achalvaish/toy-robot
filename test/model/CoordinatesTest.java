package model;

import org.junit.*;
import org.junit.runner.JUnitCore;

import static org.junit.Assert.*;

public class CoordinatesTest extends JUnitCore {

    Coordinates coordinates = new Coordinates(1,2);

    @Test
    public void testGetX() {
        assertEquals(1,coordinates.getX());
    }

    @Test
    public void testGetY() {
        assertEquals(2,coordinates.getY());
    }

    @Test
    public void testIsNull_false() {
        assertFalse(coordinates.isNull());
    }

    @Test
    public void testIsNull_true() {
        coordinates = new Coordinates();
        assertTrue(coordinates.isNull());
    }
}