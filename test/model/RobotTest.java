package model;

import Resources.Messages;
import org.junit.*;
import org.junit.runner.JUnitCore;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;


public class RobotTest extends JUnitCore {

    Robot robot = new Robot();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testValidXMove_false() {
        assertFalse(robot.validXMove(5));
    }

    @Test
    public void testValidXMove_true() {
        assertTrue(robot.validXMove(2));
    }

    @Test
    public void testValidYMove_false() {
        assertFalse(robot.validYMove(-1));
    }

    @Test
    public void testValidYMove_true() {
        assertTrue(robot.validYMove(3));
    }

    @Test
    public void testLeft_invalidMove() {
        robot.left();
        assertEquals(Messages.NOT_PLACED, outContent.toString().trim());
    }

    @Test
    public void testRight_invalidMove() {
        robot.right();
        assertEquals(Messages.NOT_PLACED, outContent.toString().trim());
    }

    @Test
    public void testMove_invalidMove_unplaced() {
        robot.move();
        assertEquals(Messages.NOT_PLACED, outContent.toString().trim());
    }

    @Test
    public void testPlace_invalidMove() {
        Coordinates coordinates  = new Coordinates(1,2);
        robot.place(coordinates,null);
        robot.report();
        assertEquals(Messages.NOT_PLACED, outContent.toString().trim());
    }

    @Test
    public void testMove_invalidMove_TableEnd_East() {
        Coordinates coordinates  = new Coordinates(Table.getWidth()-1,2);
        robot.place(coordinates,Direction.EAST);
        robot.move();
        assertEquals(Messages.END_OF_TABLE, outContent.toString().trim());
    }

    @Test
    public void testMove_invalidMove_TableEnd_West() {
        Coordinates coordinates  = new Coordinates(Table.getStartX(),2);
        robot.place(coordinates,Direction.WEST);
        robot.move();
        assertEquals(Messages.END_OF_TABLE, outContent.toString().trim());
    }

    @Test
    public void testMove_invalidMove_TableEnd_North() {
        Coordinates coordinates  = new Coordinates(4,Table.getHeight()-1);
        robot.place(coordinates,Direction.NORTH);
        robot.move();
        assertEquals(Messages.END_OF_TABLE, outContent.toString().trim());
    }

    @Test
    public void testMove_invalidMove_TableEnd_South() {
        Coordinates coordinates  = new Coordinates(4,Table.getStartY());
        robot.place(coordinates,Direction.SOUTH);
        robot.move();
        assertEquals(Messages.END_OF_TABLE, outContent.toString().trim());
    }

    @Test
    public void testReport_invalid(){
        robot.report();
        assertEquals(Messages.NOT_PLACED, outContent.toString().trim());
    }

    @Test
    public void testReport_valid(){
        Coordinates coordinates  = new Coordinates(1,2);
        Direction direction = Direction.EAST;
        robot.place(coordinates,direction);
        robot.report();
        assertEquals(coordinates.getX() + "," + coordinates.getY() + "," + direction, outContent.toString().trim());
    }
}