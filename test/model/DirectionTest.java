package model;


import Resources.Messages;
import org.junit.*;
import org.junit.runner.JUnitCore;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class DirectionTest extends JUnitCore {

    Direction direction;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void testNextValue_NORTH() {
        direction = Direction.NORTH;
        assertEquals(Direction.EAST,direction.nextValue());
    }

    @Test
    public void testNextValue_EAST() {
        direction = Direction.EAST;
        assertEquals(Direction.SOUTH,direction.nextValue());
    }

    @Test
    public void testNextValue_SOUTH() {
        direction = Direction.SOUTH;
        assertEquals(Direction.WEST,direction.nextValue());
    }

    @Test
    public void testNextValue_WEST() {
        direction = Direction.WEST;
        assertEquals(Direction.NORTH,direction.nextValue());
    }

    @Test
    public void testPrevValue_NORTH() {
        direction = Direction.NORTH;
        assertEquals(Direction.WEST,direction.prevValue());
    }

    @Test
    public void testPrevValue_EAST() {
        direction = Direction.EAST;
        assertEquals(Direction.NORTH,direction.prevValue());
    }

    @Test
    public void testPrevValue_SOUTH() {
        direction = Direction.SOUTH;
        assertEquals(Direction.EAST,direction.prevValue());
    }

    @Test
    public void testPrevValue_WEST() {
        direction = Direction.WEST;
        assertEquals(Direction.SOUTH,direction.prevValue());
    }

    @Test
    public void testGetDirection_validUpperNorth() {
        String[] input = {"1","2","NORTH"};
        assertEquals(Direction.NORTH,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validLowerNorth() {
        String[] input = {"1","2","north"};
        assertEquals(Direction.NORTH,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validUpperSouth() {
        String[] input = {"1","2","SOUTH"};
        assertEquals(Direction.SOUTH,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validLowerSouth() {
        String[] input = {"1","2","south"};
        assertEquals(Direction.SOUTH,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validUpperWest() {
        String[] input = {"1","2","WEST"};
        assertEquals(Direction.WEST,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validLowerWest() {
        String[] input = {"1","2","west"};
        assertEquals(Direction.WEST,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validUpperEast() {
        String[] input = {"1","2","EAST"};
        assertEquals(Direction.EAST,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_validLowerEAST() {
        String[] input = {"1","2","east"};
        assertEquals(Direction.EAST,Direction.getDirection(input));
    }

    @Test
    public void testGetDirection_invalid() {
        String[] input = {"1","2","NoDir"};
        assertNull(Direction.getDirection(input));
        assertEquals(Messages.INVALID_DIRECTION,outContent.toString().trim());
    }


}