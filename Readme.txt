TOY ROBOT SIMULATOR

This is a command-line application to simulate the various functions of a toy robot.

Dependencies
- Java 10
- Junit 4.13

Installation & Run instrictions

1. Compile: mvn compile
2. Test: mvn test
3. Run: mvn exec:java
4. Packaging: mvn package, compiled jar in target/ folder

Output files are included in this archive to save time while compiling & runnning

