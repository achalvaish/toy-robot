package model;

public class Coordinates {

    private int x;
    private int y;

    public Coordinates() {
        this.x = -1;
        this.y = -1;
    }
    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //Get x co-ordinate of the Robot
    public int getX() {
        return x;
    }

    //Set x co-ordinate of the Robot
    public void setX(int x) {
        this.x = x;
    }

    //Get y co-ordinate of the Robot
    public int getY() {
        return y;
    }

    //Set y co-ordinate of the Robot
    public void setY(int y) {
        this.y = y;
    }

    //Check if the co-ordinates for the robot have been initialized
    public boolean isNull(){
        return x == -1 && y == -1;
    }
}
