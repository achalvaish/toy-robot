package model;

public enum Commands {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT,
    EXIT
}
