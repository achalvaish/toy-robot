package model;

import Resources.Messages;

public class Robot{
    private Coordinates coordinates;
    private Direction direction;
    private boolean placed;

    public  Robot(){
        coordinates = new Coordinates();
        placed = false;
    }

    //Sets the co-ordinates and direction of the robot on the board
    public void place(Coordinates coordinates, Direction direction){
        if(direction != null) {
            this.coordinates.setX(coordinates.getX());
            this.coordinates.setY(coordinates.getY());
            this.direction = direction;
            placed = true;
        }
    }

    //Sets the robot to face the direction to the left of the current direction
    public void left(){
        if(placed) {
            direction = this.direction.prevValue();
        }
        else {
            System.out.println(Messages.NOT_PLACED);
        }
    }

    //Sets the robot to face the direction to the right of the current direction
    public void right(){
        if (placed) {
            direction = this.direction.nextValue();
        } else {
            System.out.println(Messages.NOT_PLACED);
        }
    }

    //Moves the robot by 1 square in the direction that the robot is facing
    public void move(){

        if(!placed) {
            System.out.println(Messages.NOT_PLACED);
            return;
        }

        int nextPosition;

        switch (direction){
            case NORTH:
                nextPosition = this.coordinates.getY() + 1;
                if (validYMove(nextPosition)) {
                    this.coordinates.setY(nextPosition);
                } else {
                    System.out.println(Messages.END_OF_TABLE);
                }
                break;
            case WEST:
                nextPosition = this.coordinates.getX() - 1;
                if (validXMove(nextPosition)) {
                    this.coordinates.setX(nextPosition);
                } else {
                    System.out.println(Messages.END_OF_TABLE);
                }
                break;
            case SOUTH:
                nextPosition = this.coordinates.getY() - 1;
                if (validYMove(nextPosition)) {
                    this.coordinates.setY(nextPosition);
                } else {
                    System.out.println(Messages.END_OF_TABLE);
                }
                break;
            case EAST:
                nextPosition = this.coordinates.getX() + 1;
                if (validXMove(nextPosition)) {
                    this.coordinates.setX(nextPosition);
                } else {
                    System.out.println(Messages.END_OF_TABLE);
                }
                break;
        }
    }

    //Obtains the current position of the robot and prints it
    public void report(){
        if (placed) {
            System.out.println(coordinates.getX() + "," + coordinates.getY() + "," + direction);
        } else {
            System.out.println(Messages.NOT_PLACED);
        }
    }

    //Checks if the input x position is valid
    public boolean validXMove(int position){
        if(position == Table.getWidth()){
            return false;
        }
        else return position >= Table.getStartX();
    }

    //Checks if the input y position is valid
    public boolean validYMove(int position){
        if(position == Table.getHeight()){
            return false;
        }
        else return position >= Table.getStartY();
    }

}