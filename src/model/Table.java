package model;

public class Table{

    private static final Integer height = 5;
    private static final Integer width = 5;
    private static final Integer startX = 0;
    private static final Integer startY = 0;

    //Obtains the height of the Table
    public static Integer getHeight() {
        return height;
    }

    //Obtains the width of the Table
    public static Integer getWidth() {
        return width;
    }

    //Obtains the starting X corner of the Table
    public static Integer getStartX() {
        return startX;
    }

    //Obtains the starting Y corner of the Table
    public static Integer getStartY() {
        return startY;
    }
}