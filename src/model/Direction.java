package model;


import Resources.Messages;

public enum Direction {
    NORTH, EAST, SOUTH, WEST;

    //Obtain the next direction value from the enum list
    public Direction nextValue() {
        return Direction.values()[(this.ordinal() + 1) % 4];
    }

    //Obtain the previous direction value from the enum list
    public Direction prevValue() {
        return Direction.values()[(this.ordinal() + 3) % 4];
    }

    //Obtain the direction value from the enum list, if not found, returns an invalid direction message
    public static Direction getDirection(String[] input) {
        try {
            return Direction.valueOf(input[2].trim().toUpperCase());
        } catch (Exception e) {
            System.out.println(Messages.INVALID_DIRECTION);
        }
        return null;

    }
}
