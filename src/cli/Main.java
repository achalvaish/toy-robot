package cli;

import Resources.Messages;
import model.*;
import static cli.CommandController.executeCommand;
import java.util.Scanner;

public class Main {

    private static void displayMessage() {
        System.out.println(Messages.PROMPT);
    }


    public static void main(String[] args) {
        Robot robot = new Robot();

        Scanner input = new Scanner(System.in);
        System.out.println("Welcome to the Toy Robot Simulator !!");

        try {
            while (true) {
                displayMessage();
                executeCommand(input.nextLine().split(" "), robot);
            }
        }catch (Exception e){
            System.out.println(e);
        }
        input.close();
    }
}
