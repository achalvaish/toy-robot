package cli;

import Resources.Messages;
import model.*;

public class CommandController {
    public static void executeCommand(String[] input, Robot robot) {

        if (input == null || robot == null)
            return;

        String inputCommand = input[0];

        try {
            switch (Commands.valueOf(inputCommand.toUpperCase())) {

                case PLACE:
                    if (input.length < 2) {
                        System.out.println(Messages.INSUFFICIENT_PARAMS);
                        break;
                    }
                    Coordinates coordinates = parseLocation(input[1]);
                    if (!coordinates.isNull())
                        robot.place(coordinates, Direction.getDirection(input[1].split(",")));
                    break;
                case MOVE:
                    robot.move();
                    break;
                case REPORT:
                    robot.report();
                    break;
                case LEFT:
                    robot.left();
                    break;
                case RIGHT:
                    robot.right();
                    break;
                case EXIT:
                    System.exit(0);
                default:
                    System.out.println(Messages.INVALID_COMMAND);
                    break;
            }
        } catch (IllegalArgumentException e) {
            System.out.println(Messages.INVALID_COMMAND);
        }
    }

    private static Coordinates parseLocation(String locationString) {
        String[] location = locationString.split(",");
        Coordinates locationCoordinates = new Coordinates();

        if (location.length < 2) {
            System.out.println(Messages.INSUFFICIENT_PARAMS);
            return locationCoordinates;
        }

        try {
            int x = Integer.parseInt(location[0]);
            int y = Integer.parseInt(location[1]);

            if (x >= Table.getWidth() || x < Table.getStartX()) {
                System.out.println(Messages.INVALID_X_PARAM);
                return locationCoordinates;
            }

            if (y >= Table.getHeight() || y < Table.getStartY()) {
                System.out.println(Messages.INVALID_Y_PARAM);
                return locationCoordinates;
            }

            locationCoordinates.setX(x);
            locationCoordinates.setY(y);
        } catch (NumberFormatException e) {
            System.out.println(Messages.INSUFFICIENT_PARAMS);
        }

        return locationCoordinates;
    }
}
