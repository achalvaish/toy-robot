package Resources;

import model.Table;

public final class Messages {

    public final static String PROMPT = "\nPlease select a command from the following list:" +
            "\nPLACE X,Y,F\n" +
            "MOVE\n" +
            "LEFT\n" +
            "RIGHT\n" +
            "REPORT\n" +
            "EXIT";
    public final static String INSUFFICIENT_PARAMS = "Invalid format for PLACE command. Please enter location in the format: PLACE x,y,direction\n"+
            Table.getStartX()+"< x< "+Table.getWidth()+"\n"+
            Table.getStartY()+"< y< "+Table.getHeight()+"\nDirection: NORTH,WEST,SOUTH,EAST";
    public final static String INVALID_DIRECTION = "Please enter a valid direction. Direction: NORTH,WEST,SOUTH,EAST";
    public final static String INVALID_X_PARAM = "Please enter valid x position. Valid x position: "+
            Table.getStartX()+"< x< "+Table.getWidth();
    public final static String INVALID_Y_PARAM = "Please enter valid y position. Valid y position: "+
            Table.getStartY()+"< y< "+Table.getHeight();
    public static final String END_OF_TABLE = "You have reached the edge of the table. Please turn to a different direction to make a move";
    public static final String NOT_PLACED="Please place the toy robot on the board to make a move";
    public static final String INVALID_COMMAND="Invalid command. Please enter a command from the above list";

}
